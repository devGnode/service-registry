FROM registry:2.7.0
MAINTAINER marco <madevincre@zenity.fr>
#
# for have no error to docker login
# remove apt-get remove golang-docker-credential-helpers
# packe : apt-get remove
#

#
# certificate SSL
#
RUN mkdir -p /certs
COPY certs/service-registry.local.key /certs/service-registry.local.key
COPY certs/service-registry.local.crt /certs/service-registry.local.crt
#
# password
#
RUN apk add --no-cache apache2-utils \
    && rm -rf /var/cache/apk/* \
    && apk add ca-certificates \
    && mkdir -p /auth \
    && htpasswd -Bnb admin 3SoNUoJTpxycLtxs > /auth/htpasswd
#
# ROOT-CACERT
#
COPY ca/ca-root-zenity.fr.crt /usr/local/share/ca-certificates/ca-root-zenity.fr.crt
COPY ca/ca-root-zenity.fr.crt /etc/ssl/certs/ca-root-zenity.fr.crt
RUN chmod ugo+rwx /etc/ssl/certs/ca-root-zenity.fr.crt \
    && update-ca-certificates
#
#
#
VOLUME ["/auth","/certs","/var/lib/registry","/usr/local/share/ca-certificates"]