#!/bin/sh

# POD_CONFIG
IMG_NAME=service-registry
POD_NAME=vpod-deploy-registry
GTE_NAME=vint-network-bridge
#
REDIRECT_PORT=5050:5000

docker run -tid --name ${POD_NAME} \
  --restart=always \
  -p ${REDIRECT_PORT} \
  -e REGISTRY_HTTP_ADDR=0.0.0.0:5000 \
  -e "REGISTRY_AUTH=htpasswd" \
  -e "REGISTRY_AUTH_HTPASSWD_REALM=Registry Realm" \
  -e REGISTRY_AUTH_HTPASSWD_PATH=/auth/htpasswd \
  -e REGISTRY_HTTP_TLS_CERTIFICATE=/certs/service-registry.local.crt \
  -e REGISTRY_HTTP_TLS_KEY=/certs/service-registry.local.key \
  ${IMG_NAME}:latest
