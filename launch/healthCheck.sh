#!/bin/sh
#
cert=
key=
cacert=
#
curl  -vvv --cacert ${cacert} --key ${key} --cert ${cert} https://zenity.service-registry.local:5050/v2/_catalog
if [ "$?" -ne "0" ]
then
    echo "heckCheck failed"
    exit 1
fi
#
echo "healthCheck success"
exit 0