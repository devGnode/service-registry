# Registry deployment

<img src="https://img.shields.io/badge/Server-DevOps-green"> <img src="https://img.shields.io/badge/vpod-Docker-blue"> <img src="https://img.shields.io/badge/Remote-172.31.1.80-red">


| host   | port   | protocol |
|---|---|---|
| https://zenity.service-registry.local | 5050:5000 | https only |

IMG_NAME : `service-registry`

POD_NAME : `vpod-deploy-egistry`

### Volume

- /auth 
- /certs
- /var/lib/registry

### add user 

````shell
htpasswd -Bnb user password >> /etc/registry/auth/htpasswd
docker restart vpod-deploy-registry
````

### setup

- deploy.sh
- healthCheck.sh

Command `echo $?` : 

- 0 success
- 1 failed

### Rest Api

url : `https://zenity.service-registry.local:5050/v2/_catalog`